from brain import Brain
import os
import json
import threading
from queue import Queue
from parse import parse

# MODE:
# 1 = analyze only with embryo, multi PV is supported
# 2 = analyze with more engines, multi PV is NOT supported
mode = 1

# relative path to the engine from cwd
#engine_path = '\\pbrain-embryo21_f.exe'
engine_path = '\\pbrain-embryo21_f.exe'

# how many games will be analyzed simultaneously
n_threads = 8

# how much time the engine has to think about one move
seconds_per_move = 1

# how many best moves to be considered
# the more moves are considered, the lower depth is reached in the same time! set higher thinking time if you want to have more pvs
multi_pv = 3

# how many times to analyze each game (to counter embryo playing different move each time)
n_repeats = 2

# whether blocking fours should be counted or not
# this only works when multi_pv > 1
count_blocking_fours = False

# how many last moves of the game should be ignored (set to 0 if you want to analyze whole games)
n_last_moves_to_ignore = 5

# whether to show the analyse of all moves in the game
show_output_for_individual_games = False

# games of which players to analyze (list), set to None if you want to analyze all games
# players_to_analyze = ['Jakub Horák', ...]
players_to_analyze = None


if mode == 2:
    multi_pv = 1
if multi_pv == 1:
    count_blocking_fours = True

output_lock = threading.Lock()

# convert the coordinates from "a1" to (0, 14) etc.
def convert_move(move):
    x = ord(move[0]) - ord('a')
    y = 15 - int(move[1:])
    return (x, y)


# analyze the given sequence of moves from the perspective of given player using the given brain
# returns n_guesses, n_top and move_stats
def analyze_game(moves, player, brain):
    board = [[0 for _ in range(15)] for _ in range(15)] # 0=empty, 1=X, 2=O
    next_symbol = 1 # X aleays starts
    my_color = None # color (X or O) of the player we want to analyze
    n_guesses = 0 # how many moves the player made
    n_top = [0 for _ in range(multi_pv)] # how many times the n-th best move was played
    move_stats = {} # (m, k): move m was k-th best
    for i, move in enumerate(moves):
        if next_symbol == my_color:
            pvs = {}
            if mode == 1: # embryo, parse PVS from the MESSAGE commands
                brain.yxboard(board, my_color)
                brain.nbest(multi_pv)
                while True: # wait until we receive a move from the engine, parse incoming messages
                    line = brain.read_line(timeout=60)
                    p = parse('{y:d},{x:d}', line)
                    if p: # we received the move
                        break
                    p = parse('MESSAGE depth {df:d}-{dt:d} multipv {n:d} ev {ev} n {_} n/ms {_} tm {_} pv {pv}', line) or\
                        parse('MESSAGE depth {df:d}-{dt:d} ev {ev} n {_} n/ms {_} tm {_} pv {pv}', line)
                    if p: # new PV was sent
                        pvn = p['n'] - 1 if 'n' in p else 0
                        pvs[pvn] = convert_move(p['pv'].split()[0])
            elif mode == 2: # other engines, only one PV, parse the move directly
                brain.board(board, my_color)
                while True: # wait until we receive a move from the engine, parse incoming messages
                    line = brain.read_line(timeout=60)
                    p = parse('{x:d},{y:d}', line)
                    if p: # we received the move
                        pvs[0] = (p['x'], p['y'])
                        break
            my_move = (move['y'], move['x']) # for some reason the X and Y coordinates are switched
            n_top_move = None
            for pvn, pv in pvs.items(): # check players move against all pvs
                if my_move == pv:
                    n_top_move = pvn
                    break
            if count_blocking_fours or len(pvs) > 1:
                n_guesses += 1
                move_stats[my_move] = n_top_move
                if n_top_move is not None:
                    n_top[n_top_move] += 1
            else:
                move_stats[my_move] = 'skipped'
        if 'swap' in move:
            if i == 3: # player #2 chose color => player #1 has the opposite of what was chosen
                fp_color = 2 if move['swap'] == 'X' else 1
            if i == 5: # player #1 chose color => he has what he chose
                fp_color = 1 if move['swap'] == 'X' else 2
            my_color = fp_color if player == 1 else 3 - fp_color # my color is determined based on if I am the player #1 or #2
        else:
            board[move['y']][move['x']] = next_symbol
            next_symbol = 3 - next_symbol # invert symbol (1 -> 2; 2 -> 1)
    return (n_guesses, n_top, move_stats)


# main function run by each thread
def thread_run(thr_id, game_queue, output_queue):
    brain = Brain(os.getcwd() + engine_path)
    brain.start(15) # 15x15 board
    brain.info('timeout_turn', seconds_per_move * 1000)
    brain.info('time_left', 10000000) # we don't want to limit the time for entire game
    brain.info('rule', 0) # we don't want to limit the time for entire game
    print(f'Thread #{thr_id} ready')

    player_stats = {} # our output
    while True:
        game = game_queue.get()
        if game == 'STOP':
            break # there are no more games to be analyzed
        for p in range(2): # analyze each player separately
            name = game['players'][p]
            if players_to_analyze is not None and name not in players_to_analyze:
                continue # we are not interested in this player
            game_stats = {'guesses': 0, 'n_top': [0 for _ in range(multi_pv)]}
            move_stats_combined = {}
            for _ in range(n_repeats):
                brain.restart() # clear the hash table etc.
                moves_to_analyze = game['moves'][:-n_last_moves_to_ignore] if n_last_moves_to_ignore else game['moves']
                guesses, n_top, move_stats = analyze_game(moves_to_analyze, p+1, brain)
                game_stats['guesses'] += guesses
                game_stats['n_top'] = [a + b for a, b in zip(game_stats['n_top'], n_top)]
                for move, top in move_stats.items():
                    if move not in move_stats_combined:
                        move_stats_combined[move] = []
                    move_stats_combined[move].append(top)
            if name not in player_stats:
                player_stats[name] = {'guesses': 0, 'n_top': [0 for _ in range(multi_pv)]}
            player_stats[name]['guesses'] += game_stats['guesses']
            player_stats[name]['n_top'] = [a + b for a, b in zip(player_stats[name]['n_top'], game_stats['n_top'])]
            with output_lock:
                print(f'{name:<25} ({game["link"]}): ', end='')
                if game_stats['guesses']:
                    top_sum = 0
                    for nt in game_stats['n_top']:
                        top_sum += nt
                        perc = top_sum / game_stats['guesses'] * 100
                        print(f'{top_sum:3d}/{game_stats["guesses"]:3d} ({perc:6.2f} %)', end='  ')
                    print()
                else:
                    print('Empty game')
                if show_output_for_individual_games:
                    for move, top in move_stats_combined.items():
                        psq_move = f'{move[1]:2d} - {move[0]:2d}'
                        if 'skipped' in top:
                            print(f'Move ({psq_move}): SKIPPED')
                        else:
                            sorted_top = sorted(top, key=lambda t:t if t is not None else 1000)
                            top_str = '/'.join(f'{t + 1}' if t is not None else 'N' for t in sorted_top)
                            print(f'Move ({psq_move}): {top_str}')

    brain.terminate()
    output_queue.put(player_stats)


def main():
    print('Loading games')
    with open('data-jednotlivci.json', 'r', encoding='utf8') as input_file: # to generate this file use scraper.py
        games = json.load(input_file)

    game_queue = Queue()
    for game in games:
        game_queue.put(game)

    print('Starting')
    threads = []
    output_queue = Queue() # get output from the threadsd through this queue
    for i in range(n_threads):
        thread = threading.Thread(target=thread_run, args=(i, game_queue, output_queue))
        thread.start()
        threads.append(thread)
        game_queue.put('STOP') # insert a stop token for the threads

    player_stats_total = {} # the final output - combined outputs from each thread
    for thread in threads:
        thread.join()
        result = output_queue.get_nowait()
        for name, score in result.items():
            if name not in player_stats_total:
                player_stats_total[name] = {'guesses': 0, 'n_top': [0 for _ in range(multi_pv)]}
            player_stats_total[name]['guesses'] += score['guesses']
            player_stats_total[name]['n_top'] = [a + b for a, b in zip(player_stats_total[name]['n_top'], score['n_top'])]

    print('-'*30)
    for name, score in sorted(player_stats_total.items(), key=lambda x: x[1]['n_top'][0] / x[1]['guesses'] if x[1]['guesses'] else 0, reverse=True): # sort according the top-move accuracy
        print(f'{name:<25}', end='')
        if score['guesses']:
            top_sum = 0
            for nt in score['n_top']:
                top_sum += nt
                perc = top_sum / score['guesses'] * 100
                print(f'{top_sum:4d}/{score["guesses"]:4d} ({perc:6.2f} %)', end='  ')
            print()
        else:
            print('No games')

if __name__ == '__main__':
    main()
