import subprocess
import threading
import time
from parse import parse

class WrongResponseError(Exception):
    def __init__(self, response):
        self.response = response
        print(self.response)

class Brain:
    def __init__(self, path):
        self.path = path
        self.process = subprocess.Popen(self.path, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        self.messages = []
        self.terminated = False
        self.reader_thread = threading.Thread(target=self.read_messages, daemon=True)
        self.reader_thread.start()

    def read_messages(self):
        while not self.terminated:
            for msg in self.process.stdout:
                self.messages.extend(msg.decode().split('\n'))

    def send(self, message):
        self.process.stdin.write((message +'\r\n').encode())
        self.process.stdin.flush()

    def start(self, board_size):
        self.send(f'START {board_size}')
        self.expect_response('OK')

    def end(self):
        self.send('END')

    def info(self, key, value):
        self.send(f'INFO {key} {value}')

    def begin(self):
        self.send('BEGIN')
    
    def turn(self, x, y):
        self.send(f'TURN {x},{y}')

    def yxboard(self, board, me, empty=0):
        bs = len(board)
        self.send('YXBOARD')
        last = None
        for x in range(bs):
            for y in range(bs):
                w = 1 if board[x][y] == me else 0 if board[x][y] == empty else 2
                if w == 2 and last is None:
                    last = (y, x, w)
                elif w:
                    self.send(f'{y},{x},{w}')
        self.send(f'{last[0]},{last[1]},{last[2]}')
        self.send('DONE')

    def board(self, board, me, empty=0):
        bs = len(board)
        self.send('BOARD')
        last = None
        for x in range(bs):
            for y in range(bs):
                w = 1 if board[x][y] == me else 0 if board[x][y] == empty else 2
                if w == 2 and last is None:
                    last = (x, y, w)
                elif w:
                    self.send(f'{x},{y},{w}')
        self.send(f'{last[0]},{last[1]},{last[2]}')
        self.send('DONE')

    def nbest(self, nbest=1):
        self.send(f'YXNBEST {nbest}')

    def terminate(self):
        state = self.process.poll()
        if state is None:
            self.end()
            self.process.wait()
        self.terminated = True

    def restart(self):
        self.send(f'RESTART')
        self.expect_response('OK')

    def read_line(self, timeout=1):
        start_time = time.time()
        while True:
            while len(self.messages) == 0:
                if time.time() - start_time > timeout:
                    return None
                time.sleep(0.01)
            line = self.messages.pop(0).strip()
            if line != '':
                return line

    def expect_response(self, expected):
        while True:
            response = self.read_line()
            if response.startswith('MESSAGE') or response.startswith('DEBUG'):
                continue
            if response != expected:
                raise WrongResponseError(response)
            break

