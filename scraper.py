import requests
from bs4 import BeautifulSoup
import json

# links = ['https://pisqworky.cz/turnaje/2293']
links = ['https://pisqworky.cz/turnaje/2279', 'https://pisqworky.cz/turnaje/2280', 'https://pisqworky.cz/turnaje/2281', 'https://pisqworky.cz/turnaje/2282', 'https://pisqworky.cz/turnaje/2283']

session = requests.Session()
session.headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36'}

duels_data = []
for link in links:
    tour_detail_page = session.get(link)
    tour_detail_soup = BeautifulSoup(tour_detail_page.text, 'html.parser')

    duel_links = tour_detail_soup.select('tr.match-duel-row a')

    for i, duel_link in enumerate(duel_links):
        print(f'{i+1}/{len(duel_links)}')
        duel_detail_page = session.get(duel_link['href'])
        duel_detail_soup = BeautifulSoup(duel_detail_page.text, 'html.parser')

        board_elem = duel_detail_soup.select_one('board')
        moves = json.loads(board_elem['moves'])
        first_player = board_elem['first-player']
        second_player = board_elem['second-player']

        result = {'moves': moves, 'players': [first_player, second_player], 'link': duel_link['href']}
        duels_data.append(result)

with open('data-jednotlivci.json', 'w+', encoding='utf8') as output_file:
    json.dump(duels_data, output_file)

